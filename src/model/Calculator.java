package model;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

public class Calculator {
    private double lastNumber;
    private double tempNumber;
    private String strNumber;
    private boolean isNewNumber;
    private String op;

    public Calculator() {
        this.lastNumber = 0;
        this.strNumber = "0.0";
        this.tempNumber = 0;
        this.isNewNumber = true;
        this.op = "";
    }

    public boolean addNewDigit(String digit) {
        if (digit==null || digit.length()!=1)
            return false;
        //Verificamos se é um digito
        if (Character.isDigit(digit.charAt(0))) {
            //Verificamos se é um número novo, caso não for adicionamos a string esse numero ex:(isNewNumber == true  --> strNUmber estava a 0,
            // isNewNUmber == false  --> strNumber estava com um valor (ex: 1234) e fica depois 12345 (supondo que digit é 5))
            if (isNewNumber)
                strNumber = digit;
            else
                strNumber += digit;
        //Se não for um digito, verifcamos se é um "ponto", se tiver verificamos se ainda não contém nenhum "ponto"
        } else if (digit.equals(".") && (isNewNumber || !strNumber.contains("."))) {
            if (isNewNumber)
                strNumber = "0";
            strNumber += ".";
        //Caso contrário não é nada
        } else
            return false;
        //Passamos o numero que esta em "String" para "Double"
        try {
            tempNumber = Double.parseDouble(strNumber);
        } catch (Exception e) {
            reset();
            return false;
        }
        isNewNumber = false;
        return true;
    }

    public boolean setOp(String newOp) {
        //Verificamos se este operacao é vazia
        if (newOp.isEmpty())
            return false;
        double value = 0;
        //Fazemos um switch do valor da operacao que ja temos para fazer, se for vazia então significa que o utilizador
        //somente colocou o primeiro valor e agora a operação, se não for vazio então a operaºão que fez já tem dois valores
        //guardados na logica da calculadora
        switch (op) {
            case "+" -> value = add(lastNumber,tempNumber);
            case "-" -> value = subtract(lastNumber,tempNumber);
            case "*" -> value = multiplication(lastNumber,tempNumber);
            case "/" -> {
                if (tempNumber==0) {
                    reset();
                    return false;
                }
                value = division(lastNumber,tempNumber);
            }
            default -> value = tempNumber;
        }

        lastNumber = tempNumber = value;
        strNumber = ""+value;
        isNewNumber = true;
        op = (newOp.equals("=") ? "" : newOp); //Se a operacao que executou for igual a "=" então passa a opercao atual a vazio, senão a operacao atual mantem se aquela que foi efetuada
        return true;
    }

    public double add(double d1,double d2){
        return d1+d2;
    }
    public double subtract(double d1,double d2){
        return d1-d2;
    }
    public double multiplication(double d1,double d2){
        return d1*d2;
    }
    public double division(double d1,double d2){
        return d1/d2;
    }
    public List<String> gerarSequenciaFibonacci() {
        if(tempNumber <= 0){
            return null;
        }
        List<String> sequencia = new ArrayList<>();
        long[][] triangle = new long[(int) tempNumber][];

        for (int i = 0; i < tempNumber; i++) {
            triangle[i] = new long[i + 1];
            triangle[i][0] = 1;

            for (int j = 1; j < i; j++) {
                triangle[i][j] = triangle[i - 1][j - 1] + triangle[i - 1][j];
            }

            triangle[i][i] = 1;
        }
        for (int i = 0; i < tempNumber; i++) {
            StringBuilder row = new StringBuilder();
            for (int j = 0; j <= i; j++) {
                row.append(triangle[i][j]);
                if (j < i) {
                    row.append(" ");
                }
            }
            sequencia.add(row.toString());
        }

        return sequencia;
    }

    public static String hexToBin(String numHex){
        String numBin;
        int intNumHex;
        try{
            intNumHex = Integer.parseInt(numHex, 16); //string to int
            numBin = Integer.toBinaryString(intNumHex);
            return numBin;
        }catch (NumberFormatException e){
            return "Error";
        }
    }

    public String binToHex(String numBin){
        String numHex;
        int intNumBin;
        try{
            intNumBin = Integer.parseInt(numBin, 2); //string to int
            numHex = Integer.toHexString(intNumBin);
            return numHex;
        }catch (NumberFormatException e){
            return "Error";
        }
    }
    public String getDisplay() {
        return strNumber;
    }

    public void perc() {
        tempNumber = tempNumber / 100 * lastNumber;
        strNumber = "" + tempNumber;
        isNewNumber = true;
    }

    public void setPi() {
        tempNumber = Math.PI;
        strNumber = ""+tempNumber;
        isNewNumber = true;
    }

    public void invertSignal() {
        tempNumber *= -1;
        strNumber = ""+tempNumber;
        isNewNumber = true;
    }
    public void reset() {
        lastNumber = 0;
        tempNumber = 0;
        strNumber  = "0.0";
        isNewNumber = true;
        op = "";
    }

    public long calcularDiferencaDias(LocalDate dataFornecida) {
        LocalDate dataAtual = LocalDate.now();
        return ChronoUnit.DAYS.between(dataAtual, dataFornecida);
    }
    public long calcularDiferencaDiasEntreDatas(LocalDate data1, LocalDate data2) {
        return ChronoUnit.DAYS.between(data1, data2);
    }

    //volume de cones e cilindros
    public double caclularVolume(double raio, double altura,boolean isCilindro) {
        if (isCilindro) {
            if (raio >= 0 && altura >= 0) {
                return Math.PI * Math.pow(raio, 2) * altura;
            } else {
                return -1;
            }
        } else {
            if (raio >= 0 && altura >= 0) {
                return (Math.PI * Math.pow(raio, 2) * altura) / 3.0;
            } else {
                return -1;
            }
        }
    }
    public double factorial(int n) {
        if (n < 0)
            throw new IllegalArgumentException("Não dá para calcular fatoriais de números negativos");
        if (n == 0 || n == 1)
            return 1;
        else
            return n * factorial(n - 1);
    }

    public void calculateFactorial() {
        try {
            int n = (int) tempNumber;
            double result = factorial(n);
            tempNumber = result;
            strNumber = String.valueOf(result);
            isNewNumber = true;
        } catch (IllegalArgumentException e) {
            reset();
            strNumber = "Error";
        }
    }
}

