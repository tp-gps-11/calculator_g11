package gui;

import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import model.Calculator;

import java.time.LocalDate;
import java.util.List;

public class RootPane {
    private VBox root;
    String [][] buttons;
    private TextField display;
    private Calculator calculator;
    private Stage stage;

    public RootPane(Calculator calculator,Stage stage) {
        this.calculator = calculator;
        this.stage = stage;

        createViews();
        registerHandlers();
        update();
    }
    private void registerHandlers() {
        for (int i = 0; i < buttons.length; i++) {
            for (int j = 0; j < buttons[0].length; j++) {
                Button button = (Button) ((HBox) root.getChildren().get(i + 1)).getChildren().get(j);
                String buttonText = buttons[i][j];
                switch (buttonText){
                    case "Volume" -> button.setOnAction(event -> handleButtonVolume());
                    case "Fibonacci"->button.setOnAction(event -> handleButtonFibo());
                    case "Conversão"-> button.setOnAction(event  -> handleConversion());
                    case "Data" -> button.setOnAction(event -> handleButtonData());
                    default-> button.setOnAction(event -> handleButtonAction(buttonText, event));

                }
            }
        }
    }
    public VBox getRoot() {
        return root;
    }
    private void createViews() {
        root = new VBox();
        root.setSpacing(5);
        root.getStyleClass().add("root");

        double preferredWidth = 500;
        double preferredHeight = 400;
        stage.setMinWidth(preferredWidth);
        stage.setMinHeight(preferredHeight);

        display = new TextField();
        display.setEditable(false);
        display.setMinHeight(100);
        display.setFont(new Font(30));
        root.getChildren().add(display);

        buttons = new String[][]{
                {"AC", "%", "+/-", "/","Volume"},
                {"7", "8", "9", "*","Fibonacci"},
                {"4", "5", "6", "-","Conversão"},
                {"1", "2", "3", "+","Data"},
                {"Pi", "0", ".", "n!","="}
        };
        for (int i = 0; i < 5; i++) {
            HBox row = new HBox();
            row.setSpacing(10);

            for (int j = 0; j < 5; j++) {
                Button button = new Button(buttons[i][j]);
                button.setMinSize(100, 80);
                row.getChildren().add(button);
            }

            root.getChildren().add(row);

        }
    }
    private void handleButtonAction(String text, ActionEvent event) {
        switch (text) {
            case "AC":
                onAC(event);
                break;
            case "%":
                onPerc(event);
                break;
            case "+/-":
                onMinusPlus(event);
                break;
            case "=","/","*","-","+":
                onOp(event);
                break;
            case "Pi":
                onPi(event);
                break;
            case "n!":
                onFactorial(event);
                break;
            default:
                onDigit(event);
        }
        update();
    }

    private void update() {
        display.setText(calculator.getDisplay());
    }
    private void handleButtonVolume() {
        Stage stage = new Stage();
        stage.setTitle("Cálculo de Volume");

        TextField radiusInput = new TextField();
        TextField heightInput = new TextField();

        ChoiceBox<String> calculationTypeChoiceBox = new ChoiceBox<>();
        calculationTypeChoiceBox.getItems().addAll("Cilindro", "Cone");
        calculationTypeChoiceBox.setValue("Cilindro");

        Button calcularVolumeBotao = new Button("Calcular Volume");
        calcularVolumeBotao.setOnAction(event -> {
            try {
                double radius = Double.parseDouble(radiusInput.getText());
                double height = Double.parseDouble(heightInput.getText());

                double result = 0;
                String selectedCalculationType = calculationTypeChoiceBox.getValue();
                if (selectedCalculationType.equals("Cilindro")) {
                    result = calculator.caclularVolume(radius, height,true);
                } else if (selectedCalculationType.equals("Cone")) {
                    result = calculator.caclularVolume(radius, height,false);
                }

                stage.close();
                display.setText(result != -1 ? String.valueOf(result) : "Error");
            } catch (NumberFormatException e) {
                System.out.println("Por favor, insira valores válidos para raio e altura.");
            }
        });

        VBox layout = new VBox();
        layout.setSpacing(10);
        layout.setPadding(new Insets(20));

        HBox hBox = new HBox();
        hBox.setSpacing(25);
        hBox.getChildren().addAll(calculationTypeChoiceBox,calcularVolumeBotao);

        layout.getChildren().addAll(
                new Label("Raio"), radiusInput,
                new Label("Altura"), heightInput,
                new Label("Tipo de Cálculo"),
                hBox
        );

        Scene scene = new Scene(layout, 300, 250);
        scene.getStylesheets().add(getClass().getResource("style/styles1.css").toExternalForm());
        stage.setScene(scene);
        stage.show();
    }

    private void handleButtonFibo() {
        List<String> sequenceList = calculator.gerarSequenciaFibonacci();
        if(sequenceList == null){
            display.setText("Error");
            return;
        }
        Stage fibonacciStage = new Stage();
        fibonacciStage.setTitle("Sequência de Fibonacci");

        VBox vbox = new VBox();
        vbox.setPadding(new Insets(10));
        vbox.setSpacing(10);

        for (String row : sequenceList) {
            Label rowLabel = new Label(row);

            rowLabel.setStyle("-fx-font-family: 'Courier New';");
            vbox.getChildren().add(rowLabel);
        }

        Scene scene = new Scene(vbox);
        scene.getStylesheets().add(getClass().getResource("style/styles1.css").toExternalForm());
        fibonacciStage.setScene(scene);

        fibonacciStage.show();
        calculator.reset();
    }
    private void onAC(ActionEvent actionEvent) {
        if(actionEvent.getSource() instanceof Button btn){
            calculator.reset();
        }
    }

    private void onPerc(ActionEvent actionEvent) {
        if(actionEvent.getSource() instanceof Button btn){
            calculator.perc();
        }
    }

    private void onMinusPlus(ActionEvent actionEvent) {
        if(actionEvent.getSource() instanceof Button btn){
            calculator.invertSignal();
        }
    }

    private void onOp(ActionEvent actionEvent) {
        if(actionEvent.getSource() instanceof Button btn){
            calculator.setOp(btn.getText());
        }
    }

    private void onDigit(ActionEvent actionEvent) {
        if(actionEvent.getSource() instanceof Button btn){
            calculator.addNewDigit(btn.getText());
        }

    }

    private void onPi(ActionEvent actionEvent) {
        if(actionEvent.getSource() instanceof Button btn){
            calculator.setPi();
        }

    }
    private void onFactorial(ActionEvent actionEvent) {
        if (actionEvent.getSource() instanceof Button btn) {
            calculator.calculateFactorial();
        }
    }


    private void handleButtonData() {
        Stage dataStage = new Stage();
        dataStage.setTitle("Cálculo de Diferença de Datas");

        VBox vbox = new VBox(10);
        vbox.setPadding(new Insets(10));

        DatePicker datePicker1 = new DatePicker();
        datePicker1.setPromptText("Selecione a data");

        DatePicker datePicker2 = new DatePicker();
        datePicker2.setPromptText("Selecione outra data");

        ChoiceBox<String> calculationTypeChoiceBox = new ChoiceBox<>();
        calculationTypeChoiceBox.getItems().addAll("Diferença até a data atual", "Diferença entre duas datas");
        calculationTypeChoiceBox.setValue("Diferença até a data atual");

        Label resultLabel = new Label();

        Button calculateButton = new Button("Calcular Diferença");
        calculateButton.setOnAction(event -> {
            String selectedCalculationType = calculationTypeChoiceBox.getValue();
            if (selectedCalculationType.equals("Diferença até a data atual")) {
                LocalDate chosenDate = datePicker1.getValue();
                if (chosenDate != null) {
                    long difference = calculator.calcularDiferencaDias(chosenDate);
                    resultLabel.setText("Diferença de dias até a data indicada: " + difference + " dias");
                } else {
                    resultLabel.setText("Por favor, selecione uma data.");
                }
            } else if (selectedCalculationType.equals("Diferença entre duas datas")) {
                LocalDate date1 = datePicker1.getValue();
                LocalDate date2 = datePicker2.getValue();

                if (date1 != null && date2 != null) {
                    long difference = calculator.calcularDiferencaDiasEntreDatas(date1, date2);
                    resultLabel.setText("Diferença de dias entre as datas: " + difference + " dias");
                } else {
                    resultLabel.setText("Por favor, selecione ambas as datas.");
                }
            }
        });

        HBox hBox = new HBox(calculationTypeChoiceBox,calculateButton);
        hBox.setSpacing(15);

        calculationTypeChoiceBox.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.equals("Diferença até a data atual")) {
                vbox.getChildren().removeAll(hBox,datePicker1,datePicker2, resultLabel);
                vbox.getChildren().addAll(hBox,datePicker1, resultLabel);
            } else if (newValue.equals("Diferença entre duas datas")) {
                vbox.getChildren().removeAll(hBox,datePicker1,datePicker2, resultLabel);
                vbox.getChildren().addAll(hBox,datePicker1,datePicker2, resultLabel);
            }
        });
        vbox.getChildren().addAll(hBox,datePicker1, resultLabel);

        Scene scene = new Scene(vbox,400,205);

        scene.getStylesheets().add(getClass().getResource("style/styles1.css").toExternalForm());
        dataStage.setScene(scene);
        dataStage.show();
    }

    private void handleConversion() {
        Stage conversionStage = new Stage();
        conversionStage.setTitle("Conversão Binário/Hexadecimal");

        TextField inputField = new TextField();
        inputField.setMinWidth(200);
        TextField outputField = new TextField();
        outputField.setMinWidth(200);
        outputField.setEditable(false);

        ChoiceBox<String> conversionTypeChoiceBox = new ChoiceBox<>();
        conversionTypeChoiceBox.getItems().addAll("Bin to Hex", "Hex to Bin");
        conversionTypeChoiceBox.setValue("Bin to Hex");

        Button convertButton = new Button("Converter");
        convertButton.setOnAction(event -> {
            String input = inputField.getText();
            String result = null;

            if (conversionTypeChoiceBox.getValue().equals("Bin to Hex")) {
                result = calculator.binToHex(input);
            } else if (conversionTypeChoiceBox.getValue().equals("Hex to Bin")) {
                result = calculator.hexToBin(input);
            }
            outputField.setText(result);
        });
        BorderPane borderPane = new BorderPane();
        borderPane.setLeft(new Label("Tipo de conversão: "));
        borderPane.setRight(conversionTypeChoiceBox);

        HBox hBox2 = new HBox(new Label("Número:    "),inputField);
        HBox hBox3 = new HBox(new Label("Resultado: "),outputField);

        VBox layout = new VBox();
        layout.setSpacing(10);
        layout.setPadding(new Insets(20));
        layout.setAlignment(Pos.CENTER);
        layout.getChildren().addAll(borderPane,hBox2,hBox3,convertButton);

        Scene scene = new Scene(layout);
        scene.getStylesheets().add(getClass().getResource("style/styles1.css").toExternalForm());

        conversionStage.setScene(scene);
        conversionStage.show();
    }
}
