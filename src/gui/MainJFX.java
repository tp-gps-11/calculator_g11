package gui;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import model.Calculator;

public class MainJFX extends Application {
    private Calculator calculator;
    @Override
    public void start(Stage stage) {
        stage.setTitle("Calculadora");
        this.calculator = new Calculator();
        stage.setResizable(false);
        RootPane calculatorPane = new RootPane(calculator,stage);

        Scene scene = new Scene(calculatorPane.getRoot());

        scene.getStylesheets().add(getClass().getResource("style/styles.css").toExternalForm());

        stage.setScene(scene);
        stage.show();
    }
}
